#include <iostream>
#include <fstream>
#include "Matrix.h"

using namespace std;

int main() {

    Matrix *matrix1 = new Matrix(5,5);
    Matrix *matrix2 = new Matrix(*matrix1);
    Matrix *matrix3 = new Matrix();

    ofstream output("out.txt");

    output << *matrix1 << endl;
    output << "Max element: " << *matrix1->getMaxItem() << endl;
    output << "Min element: " << *matrix1->getMinItem() << endl;

    matrix2->transpose();

    output << *matrix2 << endl;
    output << "Max element: " << *matrix2->getMaxItem() << endl;
    output << "Min element: " << *matrix2->getMinItem() << endl;

    *matrix3 = *matrix2 * 2;

    output << *matrix3 << endl;
    output << "Max element: " << *matrix3->getMaxItem() << endl;
    output << "Min element: " << *matrix3->getMinItem() << endl;

    output.close();

    delete matrix1;
    delete matrix2;
    delete matrix3;

    return 0;
}
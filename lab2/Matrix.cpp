#include <iostream>
#include <csignal>
#include <iomanip>
#include "Matrix.h"


/**
 * Конструктор без параметров
 * @return
 */
Matrix::Matrix() : columns(0), rows(0), data(nullptr) {
    std::cout << "Constructor without parameters" << std::endl;
}


/**
 * Конструктор с параметрами
 * @param columns   Количество столбцов
 * @param rows      Количество строк
 * @return
 */
Matrix::Matrix(uint columns, uint rows) :
        columns(columns), rows(rows), data(nullptr) {

    std::cout << "Constructor with parameters" << std::endl;

    if (columns > 0 && rows > 0) {

        float value = CPPLABS_MATRIX_FIRST;

        data = new float *[columns];

        for (uint i = 0; i < columns; ++i) {
            data[i] = new float[rows];
            for (uint j = 0; j < rows; ++j) {
                data[i][j] = value;
                value += CPPLABS_MATRIX_STEP;
            }
        }
    }
}


/**
 * Конструктор копирования
 * @param matrix Экземпляр класса Matrix
 * @return
 */
Matrix::Matrix(const Matrix &matrix) :
        columns(matrix.getColumns()), rows(matrix.getRows()), data(nullptr) {

    std::cout << "Copy constructor" << std::endl;

    const float **data = matrix.getData();

    // Проверка данных
    if (data && columns > 0 && rows > 0) {

        this->data = new float *[columns];

        // Копирование содержимого
        for (uint i = 0; i < columns; ++i) {
            this->data[i] = new float[rows];
            std::copy(data[i], data[i] + rows, this->data[i]);
        }

    }
}


/**
 * Деструктор
 */
Matrix::~Matrix() {

    std::cout << "Destructor" << std::endl;

    if (data) {
        for (uint i = 0; i < columns; ++i)
            delete[] data[i];
        delete[] data;
    }
}

/**
 * Установка количества столбцов
 * @param columns   Столбцы
 * @return          Указатель на текущий объект
 */
Matrix * Matrix::setColumns(uint columns) {
    Matrix::columns = columns;
    return this;
}


/**
 * Установка количества строк
 * @param rows      Строки
 * @return          Указатель на текущий объект
 */
Matrix * Matrix::setRows(uint rows) {
    Matrix::rows = rows;
    return this;
}


/**
 * Задание матрицы
 * @param data    Матрица
 * @return          Указатель на текущий объект
 */
Matrix * Matrix::setData(float **data) {
    Matrix::data = data;
    return this;
}


/**
 * Получение количества столбцов
 * @return Количество столбцов
 */
uint Matrix::getColumns() const {
    return columns;
}


/**
 * Получение количества строк
 * @return Количество строк
 */
uint Matrix::getRows() const {
    return rows;
}


/**
 * Получение содержимого матрицы
 * @return Содержимое матрицы
 */
const float **Matrix::getData() const {
    return (const float **) data;
}


/**
 * Транспонирование матрицы
 */
void Matrix::transpose() {

    for (uint i = 0; i < columns / 2; ++i) {
        std::swap(data[i], data[columns - 1 - i]);
    }

    for (uint i = 0; i < columns; ++i) {
        for (uint j = 0; j < rows / 2; ++j) {
            std::swap(data[i][j], data[i][rows - 1 - j]);
        }
    }
}


/**
 * Перегрузка оператора присваивания
 * @return
 */
Matrix &Matrix::operator=(Matrix &matrix) {

    std::cout << "Operator =" << std::endl;

    if (this != &matrix) {

        // Освобождение памяти
        if (data) {
            for (uint i = 0; i < columns; ++i)
                delete[] data[i];
            delete[] data;
        }

        columns = matrix.getColumns();
        rows = matrix.getRows();

        // Копирование содержимого
        const float **new_data = matrix.getData();

        data = new float *[columns];

        for (uint i = 0; i < columns; ++i) {
            data[i] = new float[rows];
            std::copy(new_data[i], new_data[i] + rows, data[i]);
        }

    }

    return *this;
}

/**
 * Перегруженный оператор умножения на число с плавоющей точкой
 * @return
 */
Matrix &Matrix::operator*(float value) {

    std::cout << "Operator *" << std::endl;

    Matrix *matrix = new Matrix();

    float **data = new float *[columns];

    for (int i = 0; i < columns; i++) {
        data[i] = new float[rows];
        for (int j = 0; j < rows; j++) {
            data[i][j] = this->data[i][j] * value;
        }
    }

    matrix->setData(data)
            ->setColumns(columns)
            ->setRows(rows);

    return *matrix;
}


/**
 * Получение максимального элемента
 * @return Указатель на максимальный элемент матрицы или nullptr
 */
const float *Matrix::getMaxItem() const {

    if (columns > 0 && rows > 0) {

        // Указатель на максимальный элемент массива
        float *value = &data[0][0];

        for (uint i = 0; i < columns; ++i ) {
            for (uint j = 0; j < rows; ++j) {
                if (*value < data[i][j]) value = &data[i][j];
            }
        }

        return value;
    }

    return nullptr;
}

/**
 * Получение минимального элемента
 * @return Указатель на минимальный элемент матрицы или nullptr
 */
const float *Matrix::getMinItem() const {

    if (columns > 0 && rows > 0) {

        // Указатель на минимальный элемент массива
        float *value = &data[0][0];

        for (uint i = 0; i < columns; ++i ) {
            for (uint j = 0; j < rows; ++j) {
                if (*value > data[i][j]) value = &data[i][j];
            }
        }

        return value;
    }

    return nullptr;
}


/**
 * Определение операции сдвига для вывода матрицы на экран
 * @return
 */
std::ostream &operator<<(std::ostream &os, Matrix &matrix) {

    os << std::endl;

    for (uint i = 0; i < matrix.columns; ++i) {
        for (uint j = 0; j < matrix.rows; ++j) {
            os << std::setw(5) << matrix.data[i][j] << "| ";
        }
        os << std::endl;
    }

    return os;
}






#ifndef CPPLABS_MATRIX_H
#define CPPLABS_MATRIX_H

#include <ostream>

#define CPPLABS_MATRIX_FIRST -10
#define CPPLABS_MATRIX_STEP 1.5

typedef unsigned int uint;

/**
 * Матрица
 */
class Matrix {

private:
    uint columns;
    uint rows;
    float **data;

private:
    uint getColumns() const;
    uint getRows() const;
    const float **getData() const;

    Matrix *setColumns(uint columns);
    Matrix *setRows(uint rows);
    Matrix *setData(float **data);

public:
    Matrix();
    Matrix(uint, uint);
    Matrix(const Matrix &);
    ~Matrix();

    void transpose();
    const float *getMinItem() const;
    const float *getMaxItem() const;

    Matrix &operator=(Matrix &);
    Matrix &operator*(float);

    friend std::ostream &operator<<(std::ostream &, Matrix &);
};

#endif //CPPLABS_MATRIX_H

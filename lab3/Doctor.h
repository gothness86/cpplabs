#ifndef CPPLABS_DOCTOR_H
#define CPPLABS_DOCTOR_H

#include "Rank.h"

/**
 * Доктор
 */
class Doctor : virtual private Rank {

protected:
    char *first_name;
    char *last_name;

public:
    ~Doctor();

    const char * getFirstName() const;
    char *getLastName() const;
    Doctor *setFirstName(const char *);
    Doctor *setLastName(const char *);

    void show();
};


#endif //CPPLABS_DOCTOR_H

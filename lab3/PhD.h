#ifndef CPPLABS_PHD_H
#define CPPLABS_PHD_H

#include "Rank.h"

/**
 * Кандидат наук
 */
class PhD : virtual private Rank {

protected:
    char *first_name;
    char *last_name;

public:
    ~PhD();

    const char * getFirstName() const;
    char *getLastName() const;
    PhD *setFirstName(const char *);
    PhD *setLastName(const char *);

    void show();

};


#endif //CPPLABS_PHD_H

#ifndef CPPLABS_ASSISTANT_H
#define CPPLABS_ASSISTANT_H

#include "Rank.h"

/**
 * Доцент
 */
class  Assistant : virtual private Rank {

protected:
    char *first_name;
    char *last_name;

public:
    ~ Assistant();

    const char * getFirstName() const;
    char *getLastName() const;
     Assistant *setFirstName(const char *);
     Assistant *setLastName(const char *);

    void show();

};


#endif //CPPLABS_ASSISTANT_H

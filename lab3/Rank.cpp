#include "Rank.h"

uint Rank::count = 0;
Rank **Rank::items = new Rank *[CPPLABS_RANK_LIMITS];

/**
 * Конструктор класса
 * @return
 */
Rank::Rank() {
    Rank::add(this);
}

/**
 * Добавление класса в список
 * @param item
 */
void Rank::add(Rank *item) {
    Rank::items[Rank::count++] = item;
}

/**
 * Отображение информации по всем классам
 */
void Rank::list() {
    for (uint i = 0; i < Rank::count; ++i) {
        Rank::items[i]->show();
    }
}

/**
 * Уничтожение классов в списке
 */
void Rank::destroy() {
    for (uint i = 0; i < Rank::count; ++i) {
        delete Rank::items[i];
    }
    Rank::count = 0;
}

#ifndef CPPLABS_RANK_H
#define CPPLABS_RANK_H

#define CPPLABS_RANK_LIMITS 25

typedef unsigned int uint;

/**
 * Звание
 */
class Rank {

private:
    static uint count;
    static Rank **items;

public:
    Rank();
    virtual ~Rank() {};

    virtual void show() {};

    static void add(Rank *);
    static void list();
    static void destroy();

};


#endif //CPPLABS_RANK_H

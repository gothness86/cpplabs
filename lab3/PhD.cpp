#include <iostream>
#include <iomanip>
#include <cstring>
#include "PhD.h"

/**
 * Деструктор
 */
PhD::~PhD() {
    std::cout << "PhD destructor" << std::endl;
    if (first_name) delete[] first_name;
    if (last_name) delete[] last_name;
}

/**
 * Получение имени
 * @return Имя
 */
const char *PhD::getFirstName() const {
    return first_name;
}

/**
 * Получение фамилии
 * @return Фамилия
 */
char *PhD::getLastName() const {
    return last_name;
}

/**
 * Установка имени
 * @param first_name    Имя
 * @return              Указатель на текущий экземпляр класса
 */
PhD *PhD::setFirstName(const char *first_name) {
    this->first_name = new char[strlen(first_name) + 1];
    strcpy(this->first_name, first_name);
    return this;
}

/**
 * Установка фамилии
 * @param last_name     Фамилия
 * @return              Указатель на экземпляр текущего класса
 */
PhD *PhD::setLastName(const char *last_name) {
    this->last_name = new char[strlen(last_name) + 1];
    strcpy(this->last_name, last_name);
    return this;
}

/**
 * Отображение информации
 */
void PhD::show() {
    std::cout << std::setw(14) << "PhD: "
              << first_name << " "
              << last_name << std::endl;
}

#include <iostream>
#include "Rank.h"
#include "Doctor.h"
#include "PhD.h"
#include "Assistant.h"

using namespace std;

int main() {

    Doctor *doctor = new Doctor();
    PhD *phd = new PhD();
    Assistant *assistant = new Assistant();

    doctor->setFirstName("Ivan")
            ->setLastName("Ivanov");

    phd->setFirstName("Vasiliy")
            ->setLastName("Petrov");

    assistant->setFirstName("Evgeniy")
            ->setLastName("Sidorov");

    Rank::list();

    Rank::destroy();

    return 0;
}
#include <iostream>
#include <iomanip>
#include <cstring>
#include "Assistant.h"


/**
 * Деструктор
 */
Assistant::~Assistant() {
    std::cout << "Assistant destructor" << std::endl;
    if (first_name) delete[] first_name;
    if (last_name) delete[] last_name;
}

/**
 * Получение имени
 * @return Имя
 */
const char *Assistant::getFirstName() const {
    return first_name;
}

/**
 * Получение фамилии
 * @return Фамилия
 */
char *Assistant::getLastName() const {
    return last_name;
}

/**
 * Установка имени
 * @param first_name    Имя
 * @return              Указатель на текущий экземпляр класса
 */
Assistant *Assistant::setFirstName(const char *first_name) {
    this->first_name = new char[strlen(first_name) + 1];
    strcpy(this->first_name, first_name);
    return this;
}

/**
 * Установка фамилии
 * @param last_name     Фамилия
 * @return              Указатель на экземпляр текущего класса
 */
Assistant *Assistant::setLastName(const char *last_name) {
    this->last_name = new char[strlen(last_name) + 1];
    strcpy(this->last_name, last_name);
    return this;
}

/**
 * Отображение информации
 */
void Assistant::show() {
    std::cout << std::setw(14) << "Assistant: "
              << first_name << " "
              << last_name << std::endl;
}

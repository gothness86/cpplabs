#ifndef CPPLABS_ADDRESS_H
#define CPPLABS_ADDRESS_H

#include <ostream>

/**
 * Адрес
 */
class Address {

private:
    char *name;
    char *street;
    int number;

public:
    char *getName() const;
    char *getStreet() const;
    int getNumber() const;

    Address *setName(const char *name);
    Address *setStreet(const char *street);
    Address *setNumber(int number);

public:
    Address();
    Address(const char *, const char *, const int);
    Address(const Address &);
    ~Address();

    friend std::ostream &operator<<(std::ostream &, Address &);

};

#endif //CPPLABS_ADDRESS_H

#include <iostream>
#include "Address.h"


using namespace std;

int main() {

    Address *address1 = new Address();

    address1->setName("Name1")
            ->setStreet("Street1")
            ->setNumber(22);

    Address *address2 = new Address("Name2", "Street2", 44);

    Address *address3 = new Address(*address2);

    address3->setName("Name3");

    cout << *address1 << endl;
    cout << *address2 << endl;
    cout << *address3 << endl;

    delete address1;
    delete address2;
    delete address3;

    return 0;

}
#include <iostream>
#include <cstring>
#include <iomanip>
#include "Address.h"

/**
 * Конструктор без параметров
 * @return
 */
Address::Address() : name(nullptr), street(nullptr), number(0) {
    std::cout << "Constructor without parameters" << std::endl;
}

/**
 * Конструктор с параметрами
 * @param name      Имя
 * @param street    Улица
 * @param number    Номер дома
 * @return
 */
Address::Address(const char *name, const char *street, const int number) :
        name(nullptr), street(nullptr), number(number) {
    std::cout << "Constructor with parameters" << std::endl;
    this->setName(name)->setStreet(street);
}

/**
 * Конструктор копирования
 * @param address Ссылка на экземпляр класса адреса
 * @return
 */
Address::Address(const Address &address) :
        name(nullptr), street(nullptr), number(address.getNumber()) {
    std::cout << "Copy constructor" << std::endl;
    this->setName(address.getName())->setStreet(address.getStreet());
}

/**
 * Деструктор
 */
Address::~Address() {
    std::cout << "Destructor for: " << name << std::endl;
    if (name) delete[](name);
    if (street) delete[](street);
}

/**
 * Получение имени
 * @return Имя
 */
char *Address::getName() const {
    return name;
}

/**
 * Получение улицы
 * @return Улица
 */
char *Address::getStreet() const {
    return street;
}

/**
 * Получение номера дома
 * @return Номер дома
 */
int Address::getNumber() const {
    return number;
}

/**
 * Установка имени
 * @param name      Имя
 * @return          Указатель на текущий объект
 */
Address *Address::setName(const char *name) {

    if (!name) return this;
    if (this->name) delete[](this->name);

    this->name = new char[strlen(name) + 1];
    strcpy(this->name, name);

    return this;
}

/**
 * Установка улицы
 * @param street    Улица
 * @return          Указатель на текущий объект
 */
Address *Address::setStreet(const char *street) {

    if (!street) return this;
    if (this->street) delete[](this->street);

    this->street = new char[strlen(street) + 1];
    strcpy(this->street, street);

    return this;
}

/**
 * Присвоение номера дома
 * @param number    Номер дома
 * @return          Указатель на текущий объект
 */
Address *Address::setNumber(int number) {
    Address::number = number;
    return this;
}

/**
 * Определение операции сдвига
 * @param os        Выходной поток
 * @param address   Ссылка на экземпляр класса адреса
 * @return
 */
std::ostream &operator<<(std::ostream &os, Address &address) {

    os << std::setw(10) << "Name: " << address.getName() << std::endl;
    os << std::setw(10) << "Street: " << address.getStreet() << std::endl;
    os << std::setw(10) << "Number: " << address.getNumber() << std::endl;

    return os;
}
